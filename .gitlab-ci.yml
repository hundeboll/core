variables:
  GIT_STRATEGY: clone
  BASE_IMAGE: debian10
  BUILD_IMAGE: "$CI_PROJECT_NAME/$BASE_IMAGE"
  REGISTRY_IMAGE: "$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/$BASE_IMAGE"

stages:
  - prepare
  - build
  - deploy

init_manifest:
  stage: prepare
  image: $CI_REGISTRY/oe-lite/bakery/$BASE_IMAGE
  script:
    - ./ci/init-manifest.sh base python multimedia
  artifacts:
    paths:
      - ci/manifest

.build:
  stage: build
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  variables:
    OE_ENV_WHITELIST: MACHINE_CPU DISTRO SDK_CPU SDK_OS
    MACHINE_CPU: i686-unknown
    DISTRO: glibc
    SDK_CPU: i686-unknown
    SDK_OS: linux-gnu
  before_script:
    - set -vx
  script:
    - docker pull $REGISTRY_IMAGE || true
    - docker build --pull
        --cache-from $REGISTRY_IMAGE
        --tag $BUILD_IMAGE setup/$BASE_IMAGE
    - docker run --rm
        -v $CI_PROJECT_DIR:$CI_PROJECT_DIR -w $CI_PROJECT_DIR/ci/manifest
        -e OE_ENV_WHITELIST="$OE_ENV_WHITELIST"
        -e MACHINE_CPU="$MACHINE_CPU" -e DISTRO="$DISTRO"
        -e SDK_CPU="$SDK_CPU" -e SDK_OS="$SDK_OS"
        $BUILD_IMAGE
        oe bake $RECIPES
  cache:
    key: "$BASE_IMAGE-$CI_JOB_NAME"
    paths:
      - ci/manifest/tmp/packages
    when: always
  tags:
    - long-run

build_native:
  extends: .build
  variables:
    RECIPES: native:world

build_arm926ejs:
  extends: .build
  variables:
    MACHINE_CPU: arm-926ejs
    RECIPES: machine:world

build_cortexa53:
  extends: .build
  variables:
    MACHINE_CPU: aarch64-cortexa53
    RECIPES: machine:world

build_ppce300c3:
  extends: .build
  variables:
    MACHINE_CPU: powerpc-e300c3
    RECIPES: machine:world

build_i686:
  extends: .build
  variables:
    MACHINE_CPU: i686-unknown
    RECIPES: machine:world

build_mingw32:
  extends: .build
  variables:
    MACHINE_CPU: i686-unknown
    DISTRO: mingw32
    RECIPES: machine:world

build_i686_sdk:
  extends: .build
  variables:
    SDK_CPU: i686-unknown
    SDK_OS: linux-gnu
    RECIPES: sdk:world

build_mingw32_sdk:
  extends: .build
  variables:
    SDK_CPU: i686-unknown
    SDK_OS: mingw32
    RECIPES: sdk:world

.deploy_docker:
  stage: deploy
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - set -vx
  script:
    - docker pull $REGISTRY_IMAGE || true
    - docker build --pull
        --cache-from $REGISTRY_IMAGE
        --tag $BUILD_IMAGE setup/$BASE_IMAGE
    - docker tag $BUILD_IMAGE $REGISTRY_IMAGE:$IMAGE_TAG
    - docker push $REGISTRY_IMAGE:$IMAGE_TAG

deploy_docker_master:
  extends: .deploy_docker
  only:
    - master
  variables:
    IMAGE_TAG: latest
  environment:
    name: docker/latest

deploy_docker_test:
  extends: .deploy_docker
  only:
    - branches
  except:
    - master
  variables:
    IMAGE_TAG: test
  environment:
    name: docker/test

deploy_docker_tag:
  extends: .deploy_docker
  only:
    - tags
  variables:
    IMAGE_TAG: $CI_COMMIT_TAG

.toolbox:
  stage: deploy
  image: quay.io/containers/buildah:v1
  variables:
    STORAGE_DRIVER: vfs
    BUILDAH_FORMAT: docker
    BUILDAH_ISOLATION: rootless
  before_script:
    - buildah login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - buildah build --pull
        --cache-from $REGISTRY_IMAGE-toolbox
        --tag $BUILD_IMAGE-toolbox
        --file Dockerfile.toolbox
        setup/$BASE_IMAGE
    - buildah tag $BUILD_IMAGE-toolbox $REGISTRY_IMAGE-toolbox:$IMAGE_TAG
    - buildah push $REGISTRY_IMAGE-toolbox:$IMAGE_TAG

toolbox_master:
  extends: .toolbox
  only:
    - master
  variables:
    IMAGE_TAG: latest

toolbox_test:
  extends: .toolbox
  only:
    - branches
  except:
    - master
  variables:
    IMAGE_TAG: test
