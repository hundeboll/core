SRC_URI += "file://0003-64-bit-multilib-hack.patch"
# The multilib-hack patch uses base_libdir environment variable
export base_libdir

SRC_URI += "file://0023-libatomic-Do-not-enforce-march-on-aarch64.patch"
SRC_URI += "file://mingw32-path-fixup.patch"
