from __future__ import absolute_import

import math
import os
import signal

from fcntl import LOCK_SH, LOCK_EX, LOCK_NB, LOCK_UN, flock
from errno import ESTALE, EWOULDBLOCK, EAGAIN, EINTR

# Simplify code below. We're using flock(2) which is not in POSIX
# anyway, so no need to try and make this completely portable.
assert EAGAIN == EWOULDBLOCK

from .compat import open_cloexec
from .util import makedirs

# This is really a somewhat generic method for implementing timeouts
# on blocking system calls that don't natively support a timeout
# parameter. Set an itimer and, if the system call fails with EINTR,
# check if the timer fired - if so, change EINTR to ETIMEDOUT (or
# report failure some other way), otherwise restart the system call.
#
# In Python, the easiest way for a signal handler to be able to mutate
# state is for it to be an instance method.
#
# We want to ensure we reset the itimer and the signal handler, so
# this only supports being used as a context manager.
#
# We do not attempt to restore the old value of the ITIMER_REAL
# timer. Also, nesting these is obviously meaningless; only use them
# for wrapping a few simple system calls, none of which muck with
# signals or fork()s or does other complicated things.
#
# Finally, in Python, only the main thread can receive and handle a
# signal, so this doesn't mix well with threads. Fortunately, Python
# also enforces that only the main thread may call signal.signal, so
# if any non-main thread ever calls the __enter__ method it will get
# an exception, and thus never enter the with block.
#
# There's a standard race condition in checking whether the timeout
# has expired and then entering a blocking system call - we avoid that
# by using the interval feature of setitimer to make sure the signal
# fires repeatedly.
class Timeout(object):
    def __init__(self, timeout):
        self.timeout = timeout

    def handler(self, signum, frame):
        self.expired = True

    # Py3
    def __bool__(self):
        return self.expired
    # Py2
    def __nonzero__(self):
        return self.__bool__()

    def __enter__(self):
        self.expired = False
        timeout = self.timeout
        if not math.isinf(timeout):
            interval = min(max(timeout/100.0, 0.01), 10.0)
            self.oldhandler = signal.signal(signal.SIGALRM, self.handler)
            signal.setitimer(signal.ITIMER_REAL, timeout, interval)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        # Disable the timer before restoring the old signal handler -
        # if we're unlucky and the timer hasn't expired, it could do
        # so just after restoring the signal handler but before
        # disabling the timer. Since the default disposition for
        # SIGALRM (and hence presumably what we're setting the handler
        # back to) is to terminate the process, that would be a pity.
        if not math.isinf(self.timeout):
            signal.setitimer(signal.ITIMER_REAL, 0)
            signal.signal(signal.SIGALRM, self.oldhandler)
        return False

class Lock(object):
    def trylock(self, shared = False):
        return self.lock(shared, 0)

class NoopLock(Lock):
    def lock(self, shared = False, timeout = None):
        return True
    def unlock(self):
        pass

class FileLock(Lock):
    def __init__(self, name):
        self.name = os.path.realpath(name)
        self.fd = -1
        self.has_lock = LOCK_UN

    def open(self):
        # Create the file if it doesn't exist, don't accept symlinks,
        flags = os.O_RDWR | os.O_CREAT | os.O_NOFOLLOW
        mode = 0o644
        assert(self.fd == -1)
        self.fd = open_cloexec(self.name, flags, mode)
        assert(self.fd >= 0)

    def close(self):
        assert(self.fd >= 0)
        os.close(self.fd)
        self.fd = -1

    def verify_inode(self):
        st1 = os.fstat(self.fd)
        try:
            st2 = os.lstat(self.name)
            if (st1.st_dev, st1.st_ino) == (st2.st_dev, st2.st_ino):
                return True
        except:
            pass
        raise OSError(ESTALE, os.strerror(ESTALE))

    def do_lock(self, flags):
        self.open()
        try:
            flock(self.fd, flags)
            self.verify_inode()
            # If we get here, we have some lock on the file on
            # disk. Keep the fd open, and record what kind of lock we
            # have.
            self.has_lock = flags & ~LOCK_NB
            return True
        except EnvironmentError as e:
            self.close()
            raise

    def lock(self, shared = False, timeout = None):
        # flock(2) allows changing the lock type held, but it is not
        # guaranteed to happen atomically - e.g., if one holds an
        # exclusive lock, one isn't guaranteed that downgrading to a
        # shared lock won't block. So we insist the the caller
        # explicitly unlocks and then locks again. This is guaranteed
        # by the assertion in self.open() above.
        flag = LOCK_SH if shared else LOCK_EX

        if timeout is None:
            timeout = float('inf')
        else:
            timeout = float(timeout)

        makedirs(os.path.dirname(self.name))

        # Always do one "trylock" operation, so that we avoid mucking
        # with signals in the presumably very common case where we can
        # get the lock immediately. If timeout is 0, this is all we
        # do. Also, if do_lock() fails in its call to self.open()
        # (e.g. permission error or some other problem
        # creating/opening the file), we should report that directly.
        try:
            return self.do_lock(flag | LOCK_NB)
        except EnvironmentError as e:
            if e.errno not in (ESTALE, EWOULDBLOCK):
                # Any kind of ENOTDIR, EPERM, EACESS or whatever else
                # open(2) might result in.
                raise e
            if timeout == 0:
                # ESTALE from verify_inode must mean that somebody had
                # or obtained a LOCK_EX and then (unlinked and)
                # unlocked before we got to do flock() on our fd. The
                # application cannot distinguish that from the case of
                # our trylock happening before the other process did
                # its unlock, which would have resulted in
                # EWOULDBLOCK. So return False in either case.
                return False
            # OK, the user requested a blocking operation, so fall
            # through to that logic.
            pass

        with Timeout(timeout) as expired:
            while not expired:
                try:
                    return self.do_lock(flag)
                except EnvironmentError as e:
                    if e.errno == ESTALE:
                        continue
                    if e.errno == EINTR and expired:
                        break
                    # Must (re)raise EINTR if we got interrupted by
                    # something other than our SIGALARM, e.g. ctrl-C.
                    raise
        return False

    def unlock(self):
        assert(self.fd >= 0)
        assert(self.has_lock in (LOCK_EX, LOCK_SH))

        # We may only unlink the file if we have an exclusive lock on
        # it. If we have a shared lock, try to upgrade, then verify
        # that we do have a lock on the file acccessible in the file
        # system. Technically, we could probably do the verify_inode()
        # inside the conditional, since if we've had an exclusive lock
        # all along, the verify_inode() we did when we got the lock
        # should be enough. But it doesn't hurt, and it's better to
        # err on the side of caution and potentially leave a lock file
        # behind.
        try:
            if self.has_lock == LOCK_SH:
                flock(self.fd, LOCK_UN)
                flock(self.fd, LOCK_EX | LOCK_NB)
            self.verify_inode()
            # Either the flock(LOCK_NB) or verify_inode() may throw an
            # exception. If not, we're entitled to unlink the file we
            # have a LOCK_EX on.
            os.unlink(self.name)
        except EnvironmentError as e:
            if e.errno not in (ESTALE, EWOULDBLOCK, EINTR):
                raise e
        finally:
            self.close()
            self.has_lock = LOCK_UN

    def __del__(self):
        # A (locked) FileLock object should not be allowed to fall out
        # of scope without unlock() being called. If it does, at least
        # avoid leaking a file descriptor.
        if self.fd >= 0:
            self.close()
